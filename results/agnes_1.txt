          used (Mb) gc trigger (Mb) max used  (Mb)
Ncells  786832 42.1    1265230 67.6   963822  51.5
Vcells 1674785 12.8    6105945 46.6 14903453 113.8
          used (Mb) gc trigger (Mb) max used  (Mb)
Ncells  786807 42.1    1265230 67.6   963822  51.5
Vcells 1674814 12.8    6105945 46.6 14903453 113.8
          used (Mb) gc trigger (Mb) max used  (Mb)
Ncells  786844 42.1    1265230 67.6  1229631  65.7
Vcells 1674810 12.8    6105945 46.6 14903453 113.8
[1] "agnes"
[1] "kmedoids-3"
[1] "kmedoids-4"
[1] "kmedoids-5"
[1] "kmedoids-6"
[1] "kmedoids-7"
[1] "kmedoids-8"
[1] "kmedoids-9"
[1] "kmedoids-12"
[1] "kmedoids-15"
[1] "kmedoids-20"
[1] "kmedoids-25"
[1] "kmedoids-30"
 18618  90825  64356  56492  62452  37499 
 [81] 57580  16781  7847   116146 67323  48341  29741  110751 19356  133384
 [91] 51515  3629   74799  84662  102701 38914  100073 24312  34409  106890
Height (summary):
    Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
     9.5   1696.0   3379.0  27750.0  10750.0 733400.0 

Available components:
[1] "order"     "height"    "ac"        "merge"     "diss"      "call"     
[7] "method"    "order.lab" "data"     
[1] "index S:"
NULL
[1] "index G1:"
NULL
[1] "index G2:"
NULL
[1] "index G3:"
NULL
