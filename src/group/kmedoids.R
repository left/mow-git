# Clustering using k-medoids algorithm 
# 
# Author: lzaremba
###############################################################################

kmedoidsForData <- function(original, data, groups = 3)
{	
	result <- pam(data, groups, keep.diss = TRUE)
	
	indexes <- calcIndexes(result$clustering, data, result$diss)
	comparison <- compareWithPrices(original, result$clustering)
	combination <- combineWithPrices(original, result$clustering)
	
	saveResults(result, indexes, comparison, combination, c(1:groups), constants.kmedoidsFiles)
	
	rm(result);
	rm(indexes);
	rm(comparison);
	rm(combination);
	gc();
}



