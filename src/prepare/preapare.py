#!/usr/bin/env python2.7

import argparse
from decimal import Decimal
from csv import DictReader, DictWriter, QUOTE_ALL


def checkIfContains(value, items):
    return 1 if value in items else 0

FIELDS_TO_REMOVE = ['Cena', 'Brutto.netto', 'Waluta', 'kW', 'Kolor', 'RejestracjaRok', 'Model', 'Wersja', 'RejestracjaMie','Kraj.aktualnej.rejestracji','Wyposazenie.dodatkowe', 'Informacje.dodatkowe', 'Skrzynia.biegow', 'PrzegladMie', 'PrzegladRok', 'UbezpieczenieMie', 'UbezpieczenieRok', 'Kraj.pochodzenia', 'ASO', 'homologacjaCiez', 'niepelnosprawni', 'nawigacja']

parser = argparse.ArgumentParser(description='Prepare data before grouping.')
parser.add_argument('input_filename', help='Name of input file, example: ../../data/cenyAutX2011.csv')
parser.add_argument('output_filename', help='Name of input file, example: ../../data/additional.csv')
args = parser.parse_args()

reader = DictReader(open(args.input_filename, 'rb'), delimiter=';', quotechar='"')
outputFieldnames = reader.fieldnames
outputFieldnames = outputFieldnames + ['ABS', 'EDS', 'ASR', '4x4', 'Komputer', 'Bezwypadkowy', 'Garazowany', 'Pierwszy.wlasciciel', 'Zabytkowy', 'Automat', 'Ubezpieczenie', 'Przeglad', 'Anglik', 'Komputer', 'klimatyzacja', 'immobiliser', 'radioCD', 'wspomaganie', 'AirBag', 'czujnikParkowania', 'ElLusterka', 'ElSzyby', 'alufelgi', 'autoalarm', 'centralnyZamek', 'ksenony', 'tempomat', 'podgrzewaneFotele', 'czujnikParkowania', 'swiatlaPrzeciwmglowe', 'czujnikDeszczu']
outputFieldnames = filter(lambda a: a not in FIELDS_TO_REMOVE, outputFieldnames)

writer = DictWriter(open(args.output_filename, 'wb'), fieldnames=outputFieldnames, delimiter=';')
writer.writeheader()

total_equipment = set()
total_info = set()

for row in reader:
    
    assert(row['Cena.w.PLN'] != 'NA' and Decimal(row['Cena.w.PLN']) > 0)
    
    #row[''] = ' '.join([row['Marka'], row['Model'], row['Wersja'], str(reader.line_num)])  
    
    #http://moto.pl/MotoPL/1,90109,10594827,Ranking_zadowolenia_z_marek___ADAC.html
    if row['Marka'] == 'Lexus':
        row['Marka'] = 1.28
    elif row['Marka'] == 'Subaru':
        row['Marka'] = 1.29
    elif row['Marka'] == 'Jaguar':
        row['Marka'] = 1.38
    elif row['Marka'] == 'Honda':
        row['Marka'] = 1.4
    elif row['Marka'] == 'Porsche':
        row['Marka'] = 1.42
    elif row['Marka'] == 'Dacia':
        row['Marka'] = 1.43
    elif row['Marka'] == 'Volvo':
        row['Marka'] = 1.46
    elif row['Marka'] == 'Saab':
        row['Marka'] = 1.48
    elif row['Marka'] == 'Mitsubishi':
        row['Marka'] = 1.48
    elif row['Marka'] == 'BMW':
        row['Marka'] = 1.48
    elif row['Marka'] == 'Toyota':
        row['Marka'] = 1.56
    elif row['Marka'] == 'Skoda':
        row['Marka'] = 1.57
    elif row['Marka'] == 'Lancia':
        row['Marka'] = 1.57
    elif row['Marka'] == 'Alfa Romeo':
        row['Marka'] = 1.58
    elif row['Marka'] == 'Audi':
        row['Marka'] = 1.59
    elif row['Marka'] == 'Daihatsu':
        row['Marka'] = 1.60
    elif row['Marka'] == 'Mazda':
        row['Marka'] = 1.60
    elif row['Marka'] == 'Nissan':
        row['Marka'] = 1.62
    elif row['Marka'] == 'Mercedes' or row['Marka'] == 'Mercedes-Benz':
        row['Marka'] = 1.65
    elif row['Marka'] == 'Citroen':
        row['Marka'] = 1.70
    elif row['Marka'] == 'Seat':
        row['Marka'] = 1.72
    elif row['Marka'] == 'Land Rover' or row['Marka'] == 'LandRover' or row['Marka'] == 'Rover':
        row['Marka'] = 1.72
    elif row['Marka'] == 'Hyundai':
        row['Marka'] = 1.72
    elif row['Marka'] == 'Suzuki':
        row['Marka'] = 1.73
    elif row['Marka'] == 'Peugeot':
        row['Marka'] = 1.77
    elif row['Marka'] == 'Ford':
        row['Marka'] = 1.79
    elif row['Marka'] == 'Volkswagen':
        row['Marka'] = 1.82
    elif row['Marka'] == 'Kia':
        row['Marka'] = 1.83
    elif row['Marka'] == 'Fiat':
        row['Marka'] = 1.83
    elif row['Marka'] == 'Renault':
        row['Marka'] = 1.83
    elif row['Marka'] == 'Opel':
        row['Marka'] = 1.83
    elif row['Marka'] == 'Mini':
        row['Marka'] = 1.87
    elif row['Marka'] == 'Smart':
        row['Marka'] = 1.92
    elif row['Marka'] == 'Chrysler':
        row['Marka'] = 1.96
    elif row['Marka'] == 'Chevrolet':
        row['Marka'] = 2.14
    else:
        continue
    
    
    if row['Przebieg.w.km'] == 'NA':
        if row['Rok.produkcji'] == '2012':
            row['Przebieg.w.km'] = 392
        elif row['Rok.produkcji'] == '2011':
            row['Przebieg.w.km'] = 7617
        elif row['Rok.produkcji'] == '2010':
            row['Przebieg.w.km'] = 20556
        elif row['Rok.produkcji'] == '2009':
            row['Przebieg.w.km'] = 50099
        elif row['Rok.produkcji'] == '2008':
            row['Przebieg.w.km'] = 92052
        elif row['Rok.produkcji'] == '2007':
            row['Przebieg.w.km'] = 107263
        elif row['Rok.produkcji'] == '2006':
            row['Przebieg.w.km'] = 120585
        elif row['Rok.produkcji'] == '2005':
            row['Przebieg.w.km'] = 132298
        elif row['Rok.produkcji'] == '2004':
            row['Przebieg.w.km'] = 142961
        elif row['Rok.produkcji'] == '2003':
            row['Przebieg.w.km'] = 151993
        elif row['Rok.produkcji'] == '2002':
            row['Przebieg.w.km'] = 160600
        elif row['Rok.produkcji'] == '2001':
            row['Przebieg.w.km'] = 166250
        elif row['Rok.produkcji'] == '2000':
            row['Przebieg.w.km'] = 171400
        elif row['Rok.produkcji'] == '1999':
            row['Przebieg.w.km'] = 177642
        elif row['Rok.produkcji'] == '1998':
            row['Przebieg.w.km'] = 181714
        elif row['Rok.produkcji'] == '1997':
            row['Przebieg.w.km'] = 189627
        elif row['Rok.produkcji'] == '1996':
            row['Przebieg.w.km'] = 193275
        elif row['Rok.produkcji'] == '1995':
            row['Przebieg.w.km'] = 198046
        elif row['Rok.produkcji'] == '1994':
            row['Przebieg.w.km'] = 201765
        elif row['Rok.produkcji'] == '1993':
            row['Przebieg.w.km'] = 201143
        elif row['Rok.produkcji'] == '1992':
            row['Przebieg.w.km'] = 252186
        else:
            row['Przebieg.w.km'] = 188000

    if (row['Przebieg.w.km'] == 'NA' or Decimal(row['Przebieg.w.km']) <= 500) and row['Rok.produkcji'] != '2011' and row['Rok.produkcji'] != '2012':
        continue
    
    if row['Przebieg.w.km'] != 'NA' and Decimal(row['Przebieg.w.km']) >= 1000000:
        continue
        
    if row['Pojemnosc.skokowa'] != 'NA' and Decimal(row['Pojemnosc.skokowa']) >= 10000:
        continue
    
    if row['Rok.produkcji'] == 'NA' or int(row['Rok.produkcji']) > 2012 or int(row['Rok.produkcji']) < 1950:
        continue
    
    if row['Rodzaj.paliwa'] == 'etanol' or row['Rodzaj.paliwa'] == 'benzyna+CNG':
        continue
    
    if row['KM'] != 'NA' and Decimal(row['KM']) < 20:
        continue
      
    assert( (row['Przebieg.w.km'] != 'NA' and Decimal(row['Przebieg.w.km']) > 500 and Decimal(row['Przebieg.w.km']) < 1000000) or ( (row['Rok.produkcji'] == '2011' or row['Rok.produkcji'] == '2012') and (row['Przebieg.w.km'] == 'NA' or Decimal(row['Przebieg.w.km']) < 1000000 ))) 
    
    if row['Brutto.netto'] == 'netto':
        row['Cena.w.PLN'] = float(row['Cena.w.PLN']) * 1.23
    
    if row['KM'] == 'NA':
        continue#row['KM'] = ''
        
    if row['Liczba.drzwi'] == '4/5':
        row['Liczba.drzwi'] = 4
    elif row['Liczba.drzwi'] == '2/3':
        row['Liczba.drzwi'] = 2
    else:
        row['Liczba.drzwi'] = ''

    if row['Status.pojazdu.sprowadzonego'] == 'sprowadzony / zarejestrowany':
        row['Status.pojazdu.sprowadzonego'] = 0
    elif row['Status.pojazdu.sprowadzonego'] == 'przygotowany do rejestracji / oplacony':
        row['Status.pojazdu.sprowadzonego'] = 242.5
    elif row['Status.pojazdu.sprowadzonego'] == 'sprowadzony / nieoplacony' and row['Pojemnosc.skokowa'] != 'NA':
        if Decimal(row['Pojemnosc.skokowa']) <= 2000 :
            row['Status.pojazdu.sprowadzonego'] = 242.5 + 500 + 17 + 160 + 169 + 70 + float(row['Cena.w.PLN']) * 0.031
        else:
            row['Status.pojazdu.sprowadzonego'] = 242.5 + 500 + 17 + 160 + 169 + 70 + float(row['Cena.w.PLN']) * 0.186
    elif row['Status.pojazdu.sprowadzonego'] == 'do sprowadzenia' and row['Pojemnosc.skokowa'] != 'NA':
        if Decimal(row['Pojemnosc.skokowa']) <= 2000 :
            row['Status.pojazdu.sprowadzonego'] = 242.5 + 500 + 17 + 160 + 169 + 70 + float(row['Cena.w.PLN']) * 0.031 + 300
        else:
            row['Status.pojazdu.sprowadzonego'] = 242.5 + 500 + 17 + 160 + 169 + 70 + float(row['Cena.w.PLN']) * 0.186 + 300
    elif row['Status.pojazdu.sprowadzonego'] == 'NA' and row['Kraj.aktualnej.rejestracji'] == 'Polska':
        row['Status.pojazdu.sprowadzonego'] = 0
    else:
        row['Status.pojazdu.sprowadzonego'] = ''
    
    if row['Pojemnosc.skokowa'] == 'NA':
        continue#row['Pojemnosc.skokowa'] = ''
    
    if row['Pojazd.uszkodzony'] == 'Tak':
        row['Pojazd.uszkodzony'] = 1
    else:
        row['Pojazd.uszkodzony'] = 0
    
    if row['Rodzaj.paliwa'] == 'olej napedowy (diesel)':
        row['Rodzaj.paliwa'] = 5.54
    elif row['Rodzaj.paliwa'] == 'benzyna':
        row['Rodzaj.paliwa'] = 5.39
    elif row['Rodzaj.paliwa'] == 'benzyna+LPG':
        row['Rodzaj.paliwa'] = 2.84
    elif row['Rodzaj.paliwa'] == 'naped elektryczny':
        row['Rodzaj.paliwa'] = 0.3
    elif row['Rodzaj.paliwa'] == 'hybryda':
        row['Rodzaj.paliwa'] = 4.3
    else: # inne, NA
        row['Rodzaj.paliwa'] = 5.33
    
    if row['UbezpieczenieMie'] != 'NA' and row['UbezpieczenieRok'] != 'NA':
        row['Ubezpieczenie'] = Decimal(row['UbezpieczenieMie']) + Decimal(row['UbezpieczenieRok']) * 12
    else:
        row['Ubezpieczenie'] = ''
    
    if row['PrzegladMie'] != 'NA' and row['PrzegladRok'] != 'NA':
        row['Przeglad'] = Decimal(row['PrzegladMie']) + Decimal(row['PrzegladRok']) * 12
    
    if row['Kraj.pochodzenia'] == 'Wielka Brytania':
        row['Anglik'] = 1
    else:
        row['Anglik'] = 0

    equipment = row['Wyposazenie.dodatkowe'].split(', ')
    total_equipment.update(equipment)
    row['ABS'] = checkIfContains('ABS', equipment)
    row['EDS'] = checkIfContains('EDS', equipment)
    row['ASR'] = checkIfContains('ASR', equipment)
    row['4x4'] = checkIfContains('4x4', equipment)
    row['Komputer'] = checkIfContains('komputer', equipment)
    row['klimatyzacja'] = checkIfContains('klimatyzacja', equipment)
    row['immobiliser'] = checkIfContains('immobiliser', equipment)
    row['radioCD'] = checkIfContains('radio / CD', equipment)
    row['wspomaganie'] = checkIfContains('wspomaganie kierownicy', equipment)
    row['AirBag'] = checkIfContains('poduszka powietrzna', equipment)
    row['czujnikParkowania'] = checkIfContains('czujnik parkowania', equipment)
    row['ElLusterka'] = checkIfContains('el. lusterka', equipment)
    row['ElSzyby'] = checkIfContains('el. szyby', equipment)
    row['alufelgi'] = checkIfContains('alufelgi', equipment)
    row['autoalarm'] = checkIfContains('autoalarm,', equipment)
    row['centralnyZamek'] = checkIfContains('centralny zamek', equipment)
    row['ksenony'] = checkIfContains('ksenony', equipment)
    row['tempomat'] = checkIfContains('tempomat', equipment)
    row['podgrzewaneFotele'] = checkIfContains('podgrzewane fotele', equipment)
    row['czujnikParkowania'] = checkIfContains('czujnik parkowania', equipment)
    row['swiatlaPrzeciwmglowe'] = checkIfContains('przeciwmglowe', equipment)
    row['czujnikDeszczu'] = checkIfContains('czujnik deszczu', equipment)
    row['nawigacja'] = checkIfContains('system nawigacji', equipment)
    
    if row['Skrzynia.biegow'] == 'automatyczna':
        row['Automat'] = 1.0
    elif ['Skrzynia.biegow'] == 'polautomatyczna / sekwencyjna':
        row['Automat'] = 0.5
    else:
        row['Automat'] = 0.0

    information = row['Informacje.dodatkowe'].split(', ')
    total_info.update(information)
    row['Bezwypadkowy'] = checkIfContains('bezwypadkowy', information)
    row['Garazowany'] = checkIfContains('garazowany', information)
    row['Pierwszy.wlasciciel'] = checkIfContains('pierwszy wlasciciel', information)
    row['Zabytkowy'] = checkIfContains('zabytkowy', information)
    row['ASO'] = checkIfContains('serwisowany w ASO', information)
    row['homologacjaCiez'] = checkIfContains('homologacja na ciezarowe', information)
    row['niepelnosprawni'] = checkIfContains('dla niepelnosprawnych', information)
    

    for field in FIELDS_TO_REMOVE:
        del(row[field])

    writer.writerow(row)

    if reader.line_num%10000 == 0:
        print reader.line_num
